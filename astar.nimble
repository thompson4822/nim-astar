task build, "Builds the program":
  setCommand "c"

task tests, "Tests the project":
    exec "nim c -r --path:src tests/all"

task release, "Build the project in release mode":
    exec "nim c src/main"

# Package

version       = "0.1.0"
author        = "Steve Thompson"
description   = "A cavern generator, using cellular automata"
license       = "MIT"
srcDir        = "src"
bin           = @["main"]

# Deps

requires "nim >= 0.16.0"
#requires "einheit >= 0.1.6"
