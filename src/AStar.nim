import sequtils
import random
import system
import math
import algorithm
import Node
import Grid

type
    # An alias for the signature needed for a heuristic function
    # I think the heuristics could be more generic if the signature
    # wasn't tied to AStar, but then again, this does help limit
    # scope
    Heuristic* = proc(first, last: Node): float

    AStar* = ref AStarObj

    # Our pathfinding engine!
    # Note, what I've done here is make everything public. I don't think this
    # is a good idea. Certainly things are easier to test, but I might be
    # able to find a better way to deal with this ...
    AStarObj = object
        open*: seq[Node]
        closed*: seq[Node]
        path*: seq[Node]
        endNode*: Node
        startNode*: Node
        grid*: Grid
        heuristic*: Heuristic

const straightCost = 1.0
const diagCost = sqrt(2.0)

# Heuristics
proc manhattan*(first, last: Node): float {.procvar.} =
    float(abs(first.x - last.x)) * straightCost + float(abs(first.y - last.y)) * straightCost

proc euclidian*(first, last: Node): float {.procvar.} =
    let dx = float(first.x - last.x)
    let dy = float(first.y - last.y)
    sqrt(dx * dx + dy * dy) * straightCost

proc diagonal*(first, last: Node): float {.procvar.} =
    let dx = float(abs(first.x - last.x))
    let dy = float(abs(first.y - last.y))
    let diag = min(dx, dy)
    let straight = dx + dy
    diagCost * diag + straightCost * (straight - 2.0 * diag)

# General AStar behavior
proc visited(a: AStar): seq[Node] =
    result = @[]
    result.add(a.closed)
    result.add(a.open)

proc buildPath(a: AStar) =
    var node = a.endNode
    a.path.add(node)
    while node != a.startNode:
        node = node.parent
        a.path.insert(node)

proc isOpen(a: AStar, node: Node): bool = a.open.contains(node)

proc isClosed(a: AStar, node: Node): bool = a.closed.contains(node)

proc updateNode(a: AStar, node: var Node, parent: Node, f, g, h: float) =
    node.f = f; node.g = g; node.h = h
    node.parent = parent

proc costFor(node1, node2: Node): float =
    # If the test node is diagonal from us, use diagonal costing
    if not ((node1.x == node2.x) or (node1.y == node2.y)):
        diagCost
    else:
        straightCost


proc findSurroundingCosts(a: AStar, node: Node) =
    # We want to make sure that we don't look at rows/cols that aren't on the
    # grid
    var startX = max(0, node.x - 1)
    var endX = min(a.grid.cols - 1, node.x + 1)
    var startY = max(0, node.y - 1)
    var endY = min(a.grid.rows - 1, node.y + 1)
    # Okay, now we can calculate the cost of the nodes that surround us.
    for row in startY .. endY:
        for col in startX .. endX:
            var test = a.grid[row][col]
            # Only calculate the cost if the node we are looking at isn't
            # the original node and the node is walkable (ie; not closed, a wall,
            # etc)
            if test != node and test.walkable:
                var g = node.g + costFor(node, test)

                var h = a.heuristic(test, a.endNode)
                var f = g + h
                if a.isOpen(test) or a.isClosed(test):
                    if test.f > f:
                        a.updateNode(test, node, f, g, h)
                else:
                    a.updateNode(test, node, f, g, h)
                    a.open.add(test)

# The heart of everything ...
proc search(a: AStar): bool =
    result = true
    var node = a.startNode
    while node != a.endNode:
        a.findSurroundingCosts(node)
        a.closed.add(node)
        if a.open.len == 0:
            echo "No Path Found"
            return false
        a.open.sort do (x, y: Node) -> int: cmp(x.f, y.f)
        # The following two lines act as shift - get the first item and then
        # remove it from the seq.
        node = a.open[a.open.low]
        a.open.delete(a.open.low)
    a.buildPath

proc createAStar*(grid: var Grid, heuristic: Heuristic): AStar =
    AStar(open: @[], closed: @[], path: @[], startNode: grid.startNode, endNode: grid.endNode, grid: grid, heuristic: heuristic)

# Kick everything off ...
proc findPath*(a: var AStar): bool =
    a.startNode.g = 0.0
    a.startNode.h = a.heuristic(a.startNode, a.endNode)
    a.startNode.f = a.startNode.g + a.startNode.h
    a.search

proc displayCharacter(solution: seq[Node], node: Node): string =
    if solution.len > 0 and solution.contains(node):
        if node == solution[0]:
            "SS"
        elif node == solution[solution.high]:
            "EE"
        else:
            "++"
    elif node.walkable:
        ".."
    else:
        "##"

proc `$`*(a: AStar): string =
    result = ""
    for row in 0 ..< a.grid.rows:
        var line = ""
        for col in 0 ..< a.grid.cols:
            line.add(displayCharacter(a.path, a.grid[row][col]))
        result.add(line); result.add("\n")
