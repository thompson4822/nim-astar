import Node
import sequtils

type
    # Holds a two dimensional array of nodes, the methods to manipulate them,
    # a start node, and an end node. It is likely that this type will go away,
    # as there are simpler ways of dealing with multidimensional arrays.
    Grid* = ref GridObj

    GridObj = object
      startNode*: Node
      endNode*: Node
      nodes*: seq[seq[Node]]

# Need to determine how this is going to create its grid.
proc createGrid*(rows, cols: int): Grid =
  var matrix = newSeqWith(rows, newSeq[Node](cols))
  for row in 0 ..< rows:
    for col in 0 ..< cols:
      matrix[row][col] = createNode(col, row)
  Grid(startNode: nil, endNode: nil, nodes: matrix)

proc `[]`*(g: Grid, i: int): seq[Node] = g.nodes[i]

# Accessors
proc cols*(g: Grid): int = g.nodes[0].len

proc rows*(g: Grid): int = g.nodes.len

proc toggleWalkable*(g: Grid, x, y, extentX, extentY: int) =
    for row in y .. y + extentY:
        for col in x .. x + extentX:
            g[row][col].walkable = not g[row][col].walkable

# Some sanity checking code. Make into tests.
# var grid = createGrid(10, 10)
# grid.startNode = grid[0][0]
# grid.endNode = grid[5][5]
# grid[0][3].walkable = true
