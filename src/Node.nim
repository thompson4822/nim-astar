
type
    # Represents a specific node evaluated as part of a pathfinding algo
    Node* = ref NodeObj
    NodeObj = object
        x*, y*: int
        f*, g*, h*: float
        walkable*: bool
        parent*: Node

# Construction
proc createNode*(col, row: int): Node =
  Node(x: col, y: row, f: 0.0, g: 0.0, h: 0.0, walkable: true, parent: nil)

proc `$`*(n: Node): string =
    "Node(x=" & $n.x & ", y=" & $n.y & ", walkable=" & $n.walkable & ")"
