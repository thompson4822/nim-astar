import sequtils
import Node
import Grid
import AStar

var grid = createGrid(40, 40)

grid.toggleWalkable(5, 10, 20, 0)
grid.toggleWalkable(26, 10, 0, 20)

grid.startNode = grid[14][15]
grid.endNode = grid[10][27]

var astar = createAStar(grid, diagonal)

discard astar.findPath

echo $astar
