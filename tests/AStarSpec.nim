import unittest, AStar, Node

suite "AStar specification":
    test "manhattan heuristic":
        # Set start node
        # Set end node
        # Check the return value
        let first = createNode(1, 1)
        let second = createNode(2, 2)
        check manhattan(first, second) == 2.0

    test "euclidian heuristic":
        # Set start node
        # Set end node
        # Check the return value
        let first = createNode(1, 1)
        let second = createNode(2, 2)
        let calculated = euclidian(first, second)
        check  calculated > 1.414 and calculated < 1.415

    test "diagonal heuristic":
        let first = createNode(1, 1)
        let second = createNode(2, 2)
        check manhattan(first, second) == 2.0

    test "visited":
        # Create a grid
        # Create AStar
        # Add some nodes to closed and open
        # Ensure that the returned seq is a combination of the nodes in both
        skip()

    test "buildPath":
        # Create a grid
        # Create AStar
        # Create start node
        # Create end node
        # ensure that path contains all nodes
        skip()

    test "isOpen returns true if node in open list": 
        # Create AStar
        # Add a node to the open list
        # Ensure true is returned
        skip()

    test "isOpen returns false if node not in open list": 
        # Create AStar
        # Ensure false is returned
        skip()

    test "isClosed returns true if node in closed list": 
        # Create AStar
        # Add a node to the closed list
        # Ensure true is returned
        skip()

    test "isClosed returns false if node not in closed list": 
        # Create AStar
        # Ensure false is returned
        skip()

    test "updateNode":
        # Ensure that the node is updated as necessary
        skip()

    test "costFor returns diagonal cost":
        # Create nodes that are diagonal to one another
        # Ensure that the diagonal cost is computed
        skip()

    test "costFor returns straight cost":
        # Create nodes that are horizontal or vertical to one another
        # Ensure that the straight cost is computed
        skip()

    test "findSurroundingCosts":
        # Create a 3x3 grid. 
        # find the costs of the nodes surrounding the middle one
        # Check that diagonal nodes have the expected cost,
        # and that vert/horizontal have the expected cost
        skip()

    test "createAStar": 
        # Create an instance
        # ensure that it is properly set up
        skip()

    test "findPath":
        # Create 3x3 grid.
        # pick two nodes
        # ensure that the path has been populated with the correct nodes.
        skip()
    
# proc displayCharacter(solution: seq[Node], node: Node): string =
#     if solution.len > 0 and solution.contains(node):
#         "+"
#     elif node.walkable:
#         "."
#     else:
#         "#"

# proc `$`*(a: AStar): string =
#     result = ""
#     for row in 0 ..< a.grid.rows:
#         var line = ""
#         for col in 0 ..< a.grid.cols:
#             line.add(displayCharacter(a.path, a.grid[row][col]))
#         result.add(line); result.add("\n")
